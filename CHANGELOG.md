WP Font Picker Changes
======================

1.0.12
------
 - Fix: PHP Warnings (Again)

1.0.11
------
 - Fix: PHP Warnings

1.0.10
------
 - Fix: css in customizer panel
 - Test with WP 6.0
 - Test with PHP 8.1

1.0.9
-----
 - Fix: plugin package slug

1.0.8
-----
 - Fix: plugin bitbucket uri

1.0.7
-----
 - Improvement: Allow external font access

1.0.6
-----
 - Refactor get_font_url()
 - Fix: ACF Field return value

1.0.5
-----
 - Fix font lib filter()
 - ACF-Field: lazy-load fontpicker.

1.0.4
-----
 - Fix PHP 8 compat

1.0.3
-----
 - Introduce filter fontpicker_enqueue_fonts

1.0.2
-----
 - Feature: introduce filter fontpicker_enqueue_fonts

1.0.1
-----
 - Feature: Introduce ACF Font Picker Field
 - Feature: introduce plugin api functions `wp_fontpicker_family_rule()` and `wp_fontpicker_font_url()`
 - Fix: Font Items not filterable after search
 - Fix: Style selectboxes not disabled if no font is selected

1.0.0
-----
 - Rebuild everything
