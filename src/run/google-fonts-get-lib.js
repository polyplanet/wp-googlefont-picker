/**
 *	Download the google font library json
 */
const exec = require('child_process');
const fs = require('fs');
const glob = require('glob');

let apiUrl = 'https://content.googleapis.com/webfonts/v1/webfonts';
let token = exec.execSync( `security find-generic-password -a $(whoami) -s "${apiUrl}" -w`, { encoding: 'utf8' } ).trim()
let url = `${apiUrl}?sort=alpha&key=${token}`

// Firefox ... copy as curl
let cmd = `curl '${url}' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:75.0) Gecko/20100101 Firefox/75.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: de,en-US;q=0.7,en;q=0.3' --compressed -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' -H 'TE: Trailers'`

let result = exec.execSync( cmd );

fs.writeFileSync( './json/google-fonts-lib.json', result );
