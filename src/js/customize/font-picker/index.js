import throttle from 'throttle';
import $ from 'jquery';

/*
Need to separate Custoizer Logic from Fontpicker Logic (lazyloading, faving)
*/

( function( api, wp, options ) {

	const fontpickerItemProps = ['name','label','selector','custom_selector','use_style','font_weight','font_style','font_family']

	const { l10n, request } = options

	const slugify = s => s.replace(/\s/g,'-').replace(/[^\w-]/g,'')

	// wp.customize.bind( 'ready', function() {
	//   wp.customize.previewer.bind( 'ready', function() {
	//      wp.customize.previewer.send( 'greeting', 'Howdy, Preview!' );
	//   } );
  // } );return;
	api.Fontpicker = api.Fontpicker || {};



	// panel //

	api.Fontpicker.PickerPanel = api.Panel.extend({
		attachEvents: function () {
			const panel = this;

			api.Panel.prototype.attachEvents.apply( this, arguments );

			// // Expand/Collapse accordion sections on click.
			panel.contentContainer.find( '.fontpicker-add-button' ).on( 'click', function( e ) {
				e.preventDefault(); // Keep this AFTER the key filter above.
				panel.addNewPickerSection()
			});
		},
		addNewPickerSection: function() {
			const panel = this;

			const newPickerSection = api.section('new_fontpicker')
			newPickerSection.activate()
			newPickerSection.expand();
		}
	})

	// section //

	api.Fontpicker.AddNewPickerSection = api.Section.extend({
		ready:function() {
			const section = this

			section.contentContainer.on( 'click', '.fontpicker-create-button', e => {
				e.preventDefault()

				const $nameCtrl = section.contentContainer.find('.new-fontpicker-name')
				const name = $nameCtrl.val()

				if ( '' === name ) {
					$nameCtrl.addClass('invalid').focus()
					return;
				}

				const data = {
					_ajax_nonce: api.settings.nonce.fontpicker_create,
					name
				}

				const request = wp.ajax.post( 'fontpicker_create', data );

				request.done( function( response ) {
					section.createClick.apply( section, [ response ] )
				});

				request.fail( function() {
					console.error(arguments)
				});

			})

			section.contentContainer.on( 'input', '.new-fontpicker-name.invalid', e => {
				e.preventDefault()
				$(e.target).removeClass('invalid')
			})
		},
		attachEvents: function () {
			api.Section.prototype.attachEvents.apply( this, arguments );

		},
		createClick:function(data) {
			const section = this,
				{ name, label } = data,
				idPrefix = `fontpicker[${name}]`

			//
			const cloneSection = api.section('fontpicker[__new_fontpicker__]')
			// clone
			const pickerSection = new api.sectionConstructor.fontpicker(
				idPrefix,
				Object.assign( {}, cloneSection.params, { id:idPrefix, title:label, instanceNumber:false } )
			)

			api.section.add( pickerSection.id, pickerSection );

			fontpickerItemProps.forEach( prop => {
				const newId = `${idPrefix}[${prop}]`
				const cloneControl = api.control(`fontpicker[__new_fontpicker__][${prop}]`)
				const cloneSetting = api.value(`fontpicker[__new_fontpicker__][${prop}]`)

				api.settings.settings[newId] = Object.assign(
					{},
					api.settings.settings[ `fontpicker[__new_fontpicker__][${prop}]` ]
				)
				api.add( new api.Setting( newId, '', {
					transport: cloneSetting.transport,
					previewer: api.previewer,
					dirty: false
				} ) )
				const params = Object.assign(
					{},
					cloneControl.params
				)

				const newControl = new cloneControl.constructor(
					newId,
					Object.assign(
						params,
						{
							id: newId,
							section: idPrefix,
							settings: { default: newId },
							content: params.content.replace( /__new_fontpicker__/g, name ),
							instanceNumber: false,
						}
					)
				)


				api.control.add( newControl.id, newControl )
				newControl.activate()
			} )
			//api.section.add( pickerSection.id, pickerSection );
			api.control(`${idPrefix}[name]`).setting(name)
			api.control(`${idPrefix}[label]`).setting(label)

			pickerSection.activate()
			pickerSection.expand()
		}

	})



	let currentPickerSection

	const onUpdateLabel = ( val, from = false ) => {
		[
			currentPickerSection.headContainer,
			currentPickerSection.contentContainer
		].forEach( $el => $el.find('.label-text').text( val ) )
	}


	const onUpdateSelector = ( val, from = false ) => {
		const customSelectorCtrl = api.control( `${currentPickerSection.id}[custom_selector]` );

		if ( from === customSelectorCtrl.setting.get() ) {
			customSelectorCtrl.setting.set(val)
		}
		if ( '' === val ) {
			customSelectorCtrl.activate();
		} else {
			customSelectorCtrl.deactivate();
		}
	}
	const onUpdateUseStyle = val => {
		const useStyleCtrl = api.control( `${currentPickerSection.id}[use_style]` );
		const styleCtrl = api.control( `${currentPickerSection.id}[font_style]` );
		const weightCtrl = api.control( `${currentPickerSection.id}[font_weight]` );
		const fontFamilyCtrl = api.control( `${currentPickerSection.id}[font_family]` );

		//$('.wp-full-overlay-sidebar-content').get(0).offsetTop
		const onScroll = e => {
			const top = Math.max(
				0,
				e.target.scrollTop
				- useStyleCtrl.container.position().top // position changes frequently
				- useStyleCtrl.container.height()
			);
			$(styleCtrl.container).add( weightCtrl.container ).css( 'top', !!top ? top + 'px' : '')
		}

		weightCtrl.container.css('top','')
		styleCtrl.container.css('top','')

		if ( val ) {
			styleCtrl.activate();
			weightCtrl.activate();
			$('.wp-full-overlay-sidebar-content').on( 'scroll', onScroll )
			// set filter
			fontFamilyCtrl.setFilter( { '400': true, '700': true } );
		} else {
			styleCtrl.deactivate();
			weightCtrl.deactivate();
			$('.wp-full-overlay-sidebar-content').off( 'scroll', onScroll )
		}
	}
	const onUpdateFamily = val => {
		let props

		const fontFamilyCtrl = api.control( `${currentPickerSection.id}[font_family]` );
		const $item = fontFamilyCtrl.container.find(`[value="${val}"]`).closest('.fontpicker-item');

		if ( !! $item.attr('data-filter-props') ) {
			props = $item.attr('data-filter-props').split('|');
		} else {
			props = [''];
		}

		// update styleCtrl + weightCtrl availability
		const styleCtrl = api.control( `${currentPickerSection.id}[font_style]` );
		const weightCtrl = api.control( `${currentPickerSection.id}[font_weight]` );

		const fn = (i,el) => {

			$(el).prop( 'disabled', ! props.includes( $(el).attr('value') ) )

			if ( props.length && ! props.includes( $(el).attr('value') ) ) {
				$(el).prop( 'selected', false );
			}
		};
		weightCtrl.container.find('option').each( fn )
		styleCtrl.container.find('option').each( fn )

	}

	api.Fontpicker.PickerSection = api.Section.extend({
		ready: function() {
			const section = this,
				ret = api.Section.prototype.ready.apply( this, arguments );

			this.contentContainer.on('click','.button-link-delete', e => {
				e.preventDefault()
				section.remove()
			})

			return ret;
		},
		remove:function() {
			const section = this,
				name = api( `${section.id}[name]` )(),
				data = {
					_ajax_nonce: api.settings.nonce.fontpicker_delete,
					name
				},
				idPrefix = `fontpicker[${name}]`,
				request = wp.ajax.post( 'fontpicker_delete', data );

			api.section(idPrefix).collapse()
			api.section(idPrefix).deactivate()

			request.done( function( response ) {
				fontpickerItemProps.forEach( prop => {
					const newId = `${idPrefix}[${prop}]`
					api.control.remove(newId)
					api.remove(newId)
				})
				api.section.remove(idPrefix)
			});

			request.fail( function() {
				console.error(arguments)
			});
		},
		onScroll: function() {

		},
		attachSettingsEvents:function() {

			currentPickerSection = this

			const labelCtrl = api.control( `${this.id}[label]` );
			const useStyleCtrl = api.control( `${this.id}[use_style]` );
			const selectorCtrl = api.control( `${this.id}[selector]` );
			const fontFamilyCtrl = api.control( `${this.id}[font_family]` );

			useStyleCtrl.setting.bind('change', onUpdateUseStyle )
			selectorCtrl.setting.bind('change', onUpdateSelector )
			fontFamilyCtrl.setting.bind('change', onUpdateFamily )
			labelCtrl.setting.bind('change', onUpdateLabel )

			// set view
			onUpdateUseStyle( useStyleCtrl.setting.get() )
			onUpdateSelector( selectorCtrl.setting.get() )
			onUpdateFamily( fontFamilyCtrl.setting.get() )

			return this;
		},
		detachSettingsEvents:function() {

			const labelCtrl = api.control( `${this.id}[label]` );
			const useStyleCtrl = api.control( `${this.id}[use_style]` );
			const selectorCtrl = api.control( `${this.id}[selector]` );
			const fontFamilyCtrl = api.control( `${this.id}[font_family]` );

			labelCtrl.setting.unbind('change', onUpdateLabel )
			useStyleCtrl.setting.unbind('change', onUpdateUseStyle )
			selectorCtrl.setting.unbind('change', onUpdateSelector )
			fontFamilyCtrl.setting.unbind('change', onUpdateFamily )

//			currentPickerSection = null

			return this;
		},
		expand: function() {
			this.attachSettingsEvents()
			return api.Section.prototype.expand.apply( this, arguments );
		},
		collapse: function() {
			this.detachSettingsEvents()
			return api.Section.prototype.collapse.apply( this, arguments );
		},
	});




	// control //
	api.Fontpicker.PickerControl = api.Control.extend({
		initialize: function( id, options ) {
			const control = this;

			return api.Control.prototype.initialize.apply( this, arguments );
		},
		getFilter:function() {
			const control = this,
				filter = {};
			control.container.find(`.fontpicker-filter-input`).forEach( (i,el) => {
				filter[ $(el).attr('value') ] = $(el).is(':checked')
			})
			return filter;
		},
		setFilter:function( filter = {} ) {
			const control = this;
			Object.entries(filter).forEach( ([prop,state]) => {
				control.container.find(`.fontpicker-filter-input[value="${prop}"]`).prop('checked',state)
			})
			return this;
		},
		ready: function() {
			api.Control.prototype.ready.apply( this, arguments );

			const control = this

			// load fonts
			api.section( control.section() ).container
				.on( 'expanded', function() {
					//control.watchFonts()
					fontpicker.lazyload(control.container,true)
				})
				.on( 'collapsed', function() {
					fontpicker.lazyload(control.container,false)
					//control.unwatchFonts()
				});

		},
	});



	/**
	 * Extends wp.customize.sectionConstructor with section constructor for fontpicker.
	 */
	$.extend( api.sectionConstructor, {
		fontpicker: api.Fontpicker.PickerSection,
		new_fontpicker: api.Fontpicker.AddNewPickerSection,
	});
	$.extend( api.controlConstructor, {
		fontpicker: api.Fontpicker.PickerControl,
	});
	$.extend( api.panelConstructor, {
		fontpicker: api.Fontpicker.PickerPanel,
	});


})( wp.customize, wp, fontpicker_customize );
