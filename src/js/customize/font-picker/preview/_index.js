(function($){

	const styleId = 'fontpicker-css'

	const ignoredFonts = ['','inherit','initial','revert','unset','dashicons','Genericons'],
		ignoredSelectors = [ /\:(before|after)$/i, /\[lang.?=.+\]/ ]

	const selectFontElements = ( value = '' ) => {
		let css = '',
			color = '#fd0'
		if ( !! value ) {
			css += `${value} { text-shadow: 1px 1px 0.1em ${color}, -1px 1px 0.1em ${color}, 1px -1px 0.1em ${color}, -1px -1px 0.1em ${color};  }
`
		}
		let $style = $(`#${styleId}`)

		if ( ! $style.length ) {
			$style = $(`<style type="text/css" id="${styleId}"></style>`).appendTo('head')
		}
		$style.html(css)
	}
	/**
	 *	reduce CSS selectors
	 */
	const reduceSelectors = selector => selector.split(',')
		.map( s => s.trim() )
		.filter( sel => {
			let i
			for ( i=0;i<ignoredSelectors.length;i++ ) {
				if ( null !== sel.match( ignoredSelectors[i] ) ) {
					return false;
				}
			}
			return true;
		} )

	/**
	 *	Whether a CSS rule should be ignored
	 */
	const ruleIsIgnored = (font, selectors) => {
		if ( ignoredFonts.includes( font ) ) {
			return true
		}

		return selectors.length === 0
	}

	wp.customize( 'fontpicker_panel', function( setting ) {
		let fonts = {}

		Array.from(document.styleSheets).forEach( style => {
			let rules, body
			try {
				rules = style.cssRules
			} catch(err) {
				return;
			}
			Array.from(rules).forEach( rule => {
				let ff, fam, selectors
				if ( !! rule.style && !! rule.style.fontFamily ) {
					fam = rule.style.fontFamily
					ff = fam.trim().replace(/['"]/g,'').split(/,\s*/g).sort().join(',')
					selectors = reduceSelectors( rule.selectorText )
					if ( ruleIsIgnored( ff, selectors ) ) {
						return
					}

					if ( ! fonts[ff] ) {
						fonts[ff] = []
					}

					fonts[ff] = fonts[ff].concat( selectors ).filter( (val,idx,arr) => arr.indexOf(val) === idx )
				}
			} )
		})


		wp.customize.preview.bind( 'control-focus', function(id) {
			console.log(id , setting.id)
			if ( id === setting.id ) {
			    setting.bind( selectFontElements );
				selectFontElements(setting.get())
			}
		})
		wp.customize.preview.bind( 'control-blur', function(id) {
			if ( id === setting.id ) {
				setting.unbind( selectFontElements );
				selectFontElements()
			}
		})

	});

})(jQuery)
