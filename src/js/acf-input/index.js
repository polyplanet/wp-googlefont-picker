import $ from 'jquery';

var Field = acf.Field.extend({

	type: 'font_family_picker',

	events: {
		'change .fontpicker-modal-cb': 'toggle',
		'change .fontpicker-value': 'collapse'
	},
	toggle: function() {
		const $accordion = this.$el.closest('.acf-accordion.-open')
		if ( this.isExpanded() ) {
			$accordion.length && $accordion.css('z-index','100000');
		} else {
			$accordion.length && $accordion.css('z-index','');
		}

	},
	initialize: function() {
		this.toggle()
		fontpicker.lazyload( this.$picker(), true )
	},
	$picker: function(){
		return this.$('.fontpicker-picker');
	},
	isExpanded: function() {
		return this.$('.fontpicker-modal-cb').is(':checked');
	},
	$input: function(){
		return this.$('.fontpicker-value:checked');
	},
	collapse: function() {
		this.$('.fontpicker-modal-cb').prop('checked',false)
	}
});

acf.registerFieldType( Field );
