import $ from 'jquery';

// favorites
$(document).on('click','.toggle-favorite',e => {
	// send ajax request
	const font = $(e.target).closest('label').prev('input').val(),
		request = wp.ajax.post( 'fontpicker_add_favorite', {
			_ajax_nonce: fontpicker.nonce.fontpicker_add_favorite,
			font
		} ),
		$container = $(e.target).closest('.fontpicker-item').parent();

	request.done( function( response ) {
		response.added.forEach( font => {
			let $el = $container.find(`[data-name="${font.toLowerCase()}"]`),
				filterProps = $el.attr('data-filter-props')
			$el.attr( 'data-filter-props', filterProps + 'favorite|' )
		})
		response.removed.forEach( font => {
			let $el = $container.find(`[data-name="${font.toLowerCase()}"]`),
				filterProps = $el.attr('data-filter-props')
			$el.attr( 'data-filter-props', filterProps.replace(/favorite\|/g,'') )
		})
	});

	request.fail( function() {
		console.error(arguments)
	});
});
