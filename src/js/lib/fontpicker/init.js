import $ from 'jquery';
import throttle from 'throttle';

$(document).ready( () => {
	$('[data-load-fontpicker]').each( (i,el) => {

		const data = JSON.parse($(el).attr('data-load-fontpicker'));

		$.post( wp.ajax.settings.url, {
			_ajax_nonce: fontpicker.nonce.fontpicker_load,
			action: 'fontpicker_load',
			...data
		} ).done( function( response ) {
			const $el = $( response );
			const $fp = $(el).parent();

			$(el).replaceWith($el);
			fontpicker.lazyload( $fp, true );
		} ).fail( console.error );
	});
});
