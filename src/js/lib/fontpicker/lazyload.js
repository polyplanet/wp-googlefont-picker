import $ from 'jquery';
import throttle from 'throttle';
import FontFaceObserver from 'fontfaceobserver';

const apiUrl = 'https://fonts.googleapis.com/css2?';
const fontsToLoad = []
const fontsLoaded = {}
const loadFontCSS = url => {
	const evt = new CustomEvent('fontpickerBeforeLoadFontCSS', {
		detail: { url }
	})
	document.dispatchEvent( evt )
	$(`<link href="${evt.detail.url}" rel="stylesheet">`).appendTo('head')
}
const loadFonts = () => {
	// let url = apiUrl;

	while ( fontsToLoad.length ) {
		/*
		if ( url.length >= 250 ) { // 500
			loadFontCSS( url )
			url = apiUrl
		}
		url += fontsToLoad.shift() + '&'
		/*/
		loadFontCSS( apiUrl + fontsToLoad.shift() )
		//*/
	}
	//loadFontCSS( url )
}

let viewObserver = false

if ( "IntersectionObserver" in window ) {
	viewObserver = new IntersectionObserver( (changes, observer) => {
		changes.forEach( change => {
			let ffo, upar
			if ( change.intersectionRatio > 0 ) {
//					console.log(change.target,$(change.target).is(':visible'))

				upar = $(change.target).attr('data-url-param')
				if ( ! upar ) {
					return
				}
				$(`[data-url-param="${upar}"]`).removeAttr('data-url-param')
				if ( !! fontsLoaded[upar] ) {
					return;
				}
				fontsToLoad.push(upar)
				fontsLoaded[upar] = true
				$(change.target).attr('data-loading',true)
				//
				ffo = new FontFaceObserver( $(change.target).attr('data-name') );
				ffo.load(null,5000).then(() => {
					$(change.target).removeAttr('data-loading')
				}).catch( err => {
					// didnt work ... dont try again
					$(change.target).removeAttr('data-loading')
					$(change.target).addClass('error')
					//$(change.target).attr('data-url-param', upar )
					//observer.observe( change.target )
				});
				observer.unobserve( change.target )
				//*
				throttle( loadFonts, 125 )
				/*/
				loadFonts()
				//*/
			}
		} )
	}, {
		root: document.querySelector('.wp-full-overlay-sidebar-content'),
		rootMargin: '0px 0px 150px 0px',
		threshold: 1.0
	});
}

module.exports = {
	lazyload: ($el,state) => {
		const $fontItems = $el.find('.fontpicker-item:not(.fontpicker-item-none)')
		$fontItems.each((i,el) => {
			const $name = $(el).find('.fontpicker-name')
			if ( $name.attr('data-url-param') ) {
				if ( !! state ) {
					// start lazyloading
					viewObserver.observe( $name.get(0) )
				} else {
					// stop lazyloading
					viewObserver.unobserve( $name.get(0) )
				}
			}
		})

	}
}
