import $ from 'jquery';
import throttle from 'throttle';

// search filter
$(document).on( 'keyup change', '.fontpicker-filter [type="search"]', e => {
	const $fontItems = $(e.target).closest('.fontpicker-filter').parent().find('.fontpicker-item:not(.fontpicker-item-none)')
	const val = $(e.target).val().toLowerCase();

	if ( '' !== val ) {
		throttle( () => {
			$fontItems.filter(`[data-name*="${val}"]`).removeAttr('style')
			$fontItems.filter(`:not([data-name*="${val}"])`).hide()
		})
	} else {
		$fontItems.removeAttr('style')
	}
})
