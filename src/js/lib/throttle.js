const throttle = (() => {
	let timeout
	return (fn,to=200,scope=false,args=[]) => {
		if ( timeout !== false ) {
			clearTimeout(timeout)
		}
		timeout = setTimeout( () => {
			if ( scope ) {
				fn.apply( scope, args )
			} else {
				fn( ... args )
			}
			timeout = false
		}, to )
	}
})()
module.exports = throttle;
