<?php

/*
Plugin Name: WP Font Picker
Plugin URI: https://bitbucket.org/polyplanet/wp-googlefont-picker
Bitbucket Plugin URI: https://bitbucket.org/polyplanet/wp-googlefont-picker
Description: Customizer the fonts typography of your blog using fonts from the google font library.
Author: POLYPLANET
Version: 1.0.17
Author URI: https://polyplanet.de
License: GPL3
Requires WP: 4.8
Requires PHP: 5.6
Text Domain: wp-font-picker
Domain Path: /languages/
*/

/*  Copyright 2020 POLYPLANET

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
Plugin was generated with Jörn Lund's WP Skelton
https://github.com/mcguffin/wp-skeleton
*/


namespace FontPicker;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}


require_once __DIR__ . DIRECTORY_SEPARATOR . 'include/autoload.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'include/api.php';

Core\Core::instance( __FILE__ );

if ( is_admin() || defined( 'DOING_AJAX' ) ) {
	Admin\Admin::instance();

}
