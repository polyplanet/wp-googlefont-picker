<?php

/**
 *	@param String $family
 *	@return Array [ 'font-family' => '"Font name", font-category' ]
 */
function wp_fontpicker_family_rule( $family ) {
	$rule = [];
	if ( $family = \FontPicker\Model\FontLibrary::instance()->findFamily( $family ) ) {
		$rule['font-family'] = $family->family_css;
	}
	return $rule;
}

/**
 *	@return String
 */
function wp_fontpicker_font_url() {
	$url = \FontPicker\Core\Core::instance()->get_font_url();

	return apply_filters( 'style_loader_src', $url, 'wp-fontpicker' );
}
