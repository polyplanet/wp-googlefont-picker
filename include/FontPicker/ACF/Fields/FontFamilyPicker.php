<?php
/**
 *	@package ACFWPObjects\Compat\Fields
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\ACF\Fields;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}


use FontPicker\Admin;
use FontPicker\Asset;
use FontPicker\Core;
use FontPicker\Model;

class FontFamilyPicker extends \acf_field {

	public static function register() {
		acf_register_field_type( get_called_class() );
	}

	/**
	 *	@inheritdoc
	 */
	function initialize() {

		// vars
		$this->name = 'font_family_picker';
		$this->label = __( 'Font Family', 'wp-font-picker' );
		$this->category = 'choice';
		$this->defaults = [
			'multiple' 		=> 0,
			'default_value'	=> '',
			'ui'			=> 0,
			'ajax'			=> 0,
			'placeholder'	=> '',

			'return_format'	=> 'object',
			'category'		=> '',
			'weights'		=> '',
			'styles'		=> '',
			'subsets'		=> ['latin'],
		];

	}




	/**
	 *	@inheritdoc
	 */
	function render_field_settings( $field ) {

		// category
		acf_render_field_setting( $field, [
			'label'			=> __('Category','wp-font-picker'),
			'instructions'	=> __('Filter font list by category','wp-font-picker'),
			'name'			=> 'category',
			'type'			=> 'select',
			'ui'			=> 0,
			'multiple'		=> 0,
			'allow_null'	=> 1,
			'choices'		=> Model\FontLibrary::$categoryNames,
		]);

		acf_render_field_setting( $field, [
			'label'			=> __('Weights','wp-font-picker'),
			'instructions'	=> __('Filter font list by weight','wp-font-picker'),
			'name'			=> 'weights',
			'type'			=> 'select',
			'ui'			=> 1,
			'multiple'		=> 1,
			'allow_null'	=> 1,
			'choices'		=> Model\FontLibrary::$weightNames,
		]);


		acf_render_field_setting( $field, [
			'label'			=> __('Styles','wp-font-picker'),
			'instructions'	=> __('Filter font list by styles','wp-font-picker'),
			'name'			=> 'styles',
			'type'			=> 'select',
			'ui'			=> 1,
			'multiple'		=> 1,
			'allow_null'	=> 1,
			'choices'		=> Model\FontLibrary::$styleNames,
		]);
		$subsets = Model\FontLibrary::instance()->subsets();
		$subset_names = array_map( function($el){
			return str_replace( '-', ' ', $el );
		}, $subsets );

		$subset_names = array_map( 'ucwords', $subsets );

		acf_render_field_setting( $field, [
			'label'			=> __('Subsets','wp-font-picker'),
			'instructions'	=> __('Filter font list by subset','wp-font-picker'),
			'name'			=> 'subsets',
			'type'			=> 'select',
			'ui'			=> 1,
			'multiple'		=> 1,
			'allow_null'	=> 1,
			'choices'		=> array_combine($subsets,$subset_names),
			'default_value'	=> ['latin'],
		]);

		// return_format
		acf_render_field_setting( $field, [
			'label'			=> __('Return Value','wp-font-picker'),
			'instructions'	=> __('Specify the returned value on front end','wp-font-picker'),
			'type'			=> 'radio',
			'name'			=> 'return_format',
			'layout'		=> 'horizontal',
			'choices'		=> [
				'css_name'		=> __( 'CSS font-family','wp-font-picker'),
				'name'			=> __( 'Family Name','wp-font-picker'),
				'object'		=> __( 'Object','wp-font-picker')
			],
		]);

	}


	/*
	 *  render_field()
	 *
	 *  Create the HTML interface for your field
	 *
	 *  @param	$field (array) the $field being rendered
	 *
	 *  @type	action
	 *  @since	3.6
	 *  @date	23/01/13
	 *
	 *  @param	$field (array) the $field being edited
	 *  @return	n/a
	 */
	function render_field( $field ) {

		//$core = Core\Core::instance();

		?>
		<div class="acf-input-wrap">
			<input type="checkbox" class="fontpicker-modal-cb" id="fontpicker-modal-open-<?php echo $field['id']; ?>">
			<div class="fontpicker-modal">
				<div class="fontpicker-picker">
					<?php

					$filter = array_intersect_key( $field, [ 'category' => '', 'weights' => '', 'styles' => '', 'subsets' ] );
					$filter = array_filter( $filter );
					printf(
						'<span data-load-fontpicker="%s"></span>',
						esc_attr( json_encode( [
							'input_name'		=> $field['name'],
							'input_id'			=> $field['id'],
							'selected_family'	=> $field['value'],
							'input_extra_attr'	=> '',
							'font_filter'		=> $filter,
						] ) )
					);
//					Core\Renderer::render_font_picker( $field['name'], $field['id'], $field['value'], '', $filter );

					?>
				</div>
				<label for="fontpicker-modal-open-<?php echo $field['id']; ?>" class="dashicons dashicons-edit">
					<span class="screen-reader-text"><?php esc_html_e('Open Font Select','wp-font-picker'); ?></span>
				</label>
				<label for="fontpicker-modal-open-<?php echo $field['id']; ?>" class="dashicons dashicons-no">
					<span class="screen-reader-text"><?php esc_html_e('Close Font Select','wp-font-picker'); ?></span>
				</label>
			</div>
		</div>
		<?php
	}


	/*
	 *  input_admin_enqueue_scripts()
	 *
	 *  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	 *  Use this action to add CSS + JavaScript to assist your render_field() action.
	 *
	 *  @type	action (admin_enqueue_scripts)
	 *  @since	3.6
	 *  @date	23/01/13
	 *
	 *  @param	n/a
	 *  @return	n/a
	 */
	function input_admin_enqueue_scripts() {

		// wp_enqueue_media();

		// Asset\Asset::get('js/admin/sweet-spot.js')
		// 	->deps('acf-input')
		// 	->enqueue();
		$admin = Admin\Admin::instance();

		Asset\Asset::get('js/acf-input.js')
			->localize( [
			], 'fontpicker_acf' )
			->add_dep( $admin->fontpicker_js->handle )
			->enqueue();

		Asset\Asset::get('css/fontpicker.css')
			->enqueue();
	}

	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add CSS + JavaScript to assist your render_field_options() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/
	// function field_group_admin_enqueue_scripts() {
	//
	// //	wp_enqueue_media();
	//
	// }


	/*
	*  load_value()
	*
	*  This filter is applied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value found in the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*  @return	$value
	*/
	// function load_value( $value, $post_id, $field ) {
	//
	// 	// prepare data for display
	//
	// 	return $value;
	// }


	/**
	 *	@inheritdoc
	 */
	function format_value( $value, $post_id, $field ) {

		// bail ealry if is empty
		if ( acf_is_empty( $value ) ) {
			return $value;
		}

		if ( $field['return_format'] == 'name' ) {
			// do nothing
			return $value;
		}
		$lib = Model\FontLibrary::instance();
		$family = $lib->findFamily( $value );

		// value
		if( $field['return_format'] == 'css_name' ) {
			$value = $family->family_css;;
		} else if ( $field['return_format'] == 'object' ) {
			$value = $family;
		}
		// return
		return $value;

	}

}
