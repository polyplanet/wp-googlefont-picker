<?php
/**
 *	@package FontPicker\Admin
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Admin;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Ajax;
use FontPicker\Asset;
use FontPicker\Core;


class Admin extends Core\Singleton {

	/** @var Ajax\AjaxHandler */
	private $_favs_handler = null;

	/** @var Ajax\AjaxHandler */
	private $_create_handler = null;

	/** @var Ajax\AjaxHandler */
	private $_delete_handler = null;

	/** @var Ajax\AjaxHandler */
	private $_load_handler = null;

	/** @var Core\Core */
	private $core;

	/** @var Asset\Asset */
	public $fontpicker_js;

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		$this->core = Core\Core::instance();

		$this->_favs_handler = new Ajax\AjaxHandler( 'fontpicker_add_favorite', [
			'callback' 		=> [ $this, 'ajax_add_favorite' ],
			'capability'	=> 'edit_theme_options',
			'public'		=> false,
			'use_nonce'		=> true,
		] );

		$this->_create_handler = new Ajax\AjaxHandler( 'fontpicker_create', [
			'callback' 		=> [ $this, 'ajax_create_fontpicker' ],
			'capability'	=> 'edit_theme_options',
			'public'		=> false,
			'use_nonce'		=> true,
		] );

		$this->_delete_handler = new Ajax\AjaxHandler( 'fontpicker_delete', [
			'callback' 		=> [ $this, 'ajax_delete_fontpicker' ],
			'capability'	=> 'edit_theme_options',
			'public'		=> false,
			'use_nonce'		=> true,
		] );

		$this->_load_handler = new Ajax\AjaxHandler( 'fontpicker_load', [
			'callback' 		=> [ $this, 'ajax_load_fontpicker' ],
			'capability'	=> 'edit_theme_options',
			'public'		=> false,
			'use_nonce'		=> true,
		] );

		$this->fontpicker_js = Asset\Asset::get('js/fontpicker.js');

		if ( did_action('init') ) {
			$this->init();
		} else {
			add_action( 'init', [ $this , 'init' ] );
		}

		add_action( 'admin_print_scripts', [ $this , 'enqueue_assets' ] );

		add_filter( 'customize_refresh_nonces', [ $this, 'customize_refresh_nonces' ] );
		add_filter( 'wp_refresh_nonces', [ $this, 'customize_refresh_nonces' ] );

	}


	public function customize_refresh_nonces( $nonces ) {

		$nonces[ 'fontpicker_add_favorite' ] = $this->favs_handler->nonce;
		$nonces[ 'fontpicker_create' ] = $this->create_handler->nonce;
		$nonces[ 'fontpicker_delete' ] = $this->delete_handler->nonce;
		$nonces[ 'fontpicker_load' ] = $this->load_handler->nonce;

		return $nonces;
	}

	// Y?
	public function __get( $what ) {
		if ( 'favs_handler' === $what ) {
			return $this->_favs_handler;
		} else if ( 'create_handler' === $what ) {
			return $this->_create_handler;
		} else if ( 'delete_handler' === $what ) {
			return $this->_delete_handler;
		} else if ( 'load_handler' === $what ) {
			return $this->_load_handler;
		}
	}




	/**
	 *	@return Array map css selector to label [
	 *		'selector'	=> 'Selector label',
	 *		'.some-class' => 'Some Class',
	 * ]
	 */
	public function get_available_css_selectors() {
		$selectors = [
			'body'
				=> __( 'Base Font', 'wp-font-picker' ),

			'.site-title'
				=> __( 'Site Title', 'wp-font-picker' ),

			'.site-title,.entry-title,.site-description,.widget-title'
				=> __( 'All Titles', 'wp-font-picker' ),

			'#primary-menu .menu-item'
				=> __( 'Main Menu', 'wp-font-picker' ),

			'h1,h2,h3,h4,h5,h6'
				=> __( 'Headlines', 'wp-font-picker' ),

			'.site-header'
				=> __( 'Site Header', 'wp-font-picker' ),

			'.site-content'
				=> __( 'Site Content', 'wp-font-picker' ),

			'.site-footer'
				=> __( 'Site Footer', 'wp-font-picker' ),

			''	=> __( 'Custom CSS', 'wp-font-picker' ),
		];
		return apply_filters( 'fontpicker_selectors', $selectors );
	}
	public function get_default_css_selector() {
		return '.site-title,.entry-title,.site-description,.widget-title';
	}

	/**
	 *	Admin init
	 *	@action admin_init
	 */
	public function init() {

		$this->fontpicker_js
			->localize([
					'nonce'	=> [
						'fontpicker_add_favorite' => $this->favs_handler->nonce,
						'fontpicker_load'         => $this->load_handler->nonce,
					]
				],
				'fontpicker'
			)
			->register();

	}

	/**
	 *	Enqueue options Assets
	 *	@action admin_print_scripts
	 */
	public function enqueue_assets() {

	}

	/**
	 *	ajax callback
	 *	@param array $data
	 */
	public function ajax_load_fontpicker( $data ) {
		header( 'Content-Type: text/html' );
		$data = wp_parse_args( $data, [
			'input_name' => '',
			'input_id' => '',
			'selected_family' => '',
			'input_extra_attr' => '',
			'font_filter' => [],
		] );

		$data['font_filter'] = wp_parse_args( $data['font_filter'], [ 'subsets' => 'latin' ] );

		Core\Renderer::render_font_picker( $data['input_name'], $data['input_id'], $data['selected_family'], $data['input_extra_attr'], $data['font_filter'] );
		exit();
	}

	/**
	 *	ajax callback
	 *	@param array $data
	 */
	public function ajax_add_favorite( $data ) {

		if ( ! isset( $data['font'] ) || empty( $data['font'] ) ) {
			return [
				'success' => false,
				'message' => __( 'Name not set or empty', 'wp-googefont-picker' ),
			];
		}
		$fonts = (array) $data['font'];
		$favorites = get_user_option( 'googlefont_favorites' ); // legacy name...
		if ( ! $favorites ) {
			$favorites = [];
		}
		$added = [];
		$removed = [];
		foreach ( $fonts as $font ) {
			if ( $pos = array_search( $font, $favorites ) ) {
				unset( $favorites[ $pos ]);
				$removed[] = $font;
			} else {
				$favorites[] = $font;
				$added[] = $font;
			}
		}

		$favorites = array_unique( $favorites );
		$favorites = array_filter( $favorites );

		update_user_option( get_current_user_id(), 'googlefont_favorites', $favorites );

		return [
			'success' => true,
			'data' => [
				'removed' => $removed,
				'added' => $added,
			],
		];
	}


	/**
	 *	ajax callback
	 *	@param array $data
	 *	@return
	 */
	public function ajax_delete_fontpicker( $data ) {

		if ( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
			return [
				'success' => false,
				'message' => __( 'Name not set or empty', 'wp-googefont-picker' ),
			];
		}

		$key = sanitize_key( $data['name'] );

		$fontpicker = get_theme_mod('fontpicker');

		if ( ! isset( $fontpicker[ $key ] ) ) {
			return [
				'success' => false,
				'message' => __( 'Name not set or empty', 'wp-googefont-picker' ),
			];
		}

		$label = $fontpicker[ $key ]['label'];

		unset( $fontpicker[ $key ] );

		set_theme_mod( 'fontpicker', $fontpicker );

		return [
			'success' => true,
			'message' => sprintf( __( 'Fontpicker %s deleted.', 'wp-googefont-picker' ), $label ),
		];
	}

	/**
	 *	ajax callback
	 *	@param array $data
	 *	@return
	 */
	public function ajax_create_fontpicker( $data ) {

		if ( ! isset( $data['name'] ) || empty( $data['name'] ) ) {
			return [
				'success' => false,
				'message' => __( 'Name not set or empty', 'wp-googefont-picker' ),
			];
		}

		$key_base = $key = sanitize_key( $data['name'] );

		$fontpicker = get_theme_mod('fontpicker');

		if ( ! is_array( $fontpicker ) ) {
			$fontpicker = [];
		}

		// make sure key is unique
		$i = 0;
		while ( isset( $fontpicker[ $key ] ) ) {
			$key = sprintf( '%s-%d', $key_base, ++$i );
		}

		$fontpicker[ $key ] = [
			'name' => $key,
			'label' => $data['name'],
		];

		set_theme_mod( 'fontpicker', $fontpicker );

		return [
			'success' => true,
			'data' => $fontpicker[ $key ],
		];
	}
}
