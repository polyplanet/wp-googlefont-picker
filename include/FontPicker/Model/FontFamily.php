<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Model;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class FontFamily {

	/** @var array */
	private $variants = [];

	/** @var array */
	private $subsets = [];

	/** @var array */
	private $weights = [];

	/** @var array */
	private $styles = [];

	/** @var string */
	private $name = '';

	/** @var string */
	private $slug = '';

	/** @var string */
	private $category = '';

	/**
	 *	@param object $data Item from font library items array
	 */
	public function __construct( $data ) {

		$this->category = $data->category;

		$this->name = $data->family;

		$this->subsets = $data->subsets;

		$this->slug = preg_replace( '/[^0-9a-z]/imsU', '', strtolower( $this->name ) );

		// add variants
		foreach ( $data->variants as $variant ) {
			$font_variant = new FontVariant( $variant, $this );
			$this->variants[ $font_variant->id ] = $font_variant;
			$this->weights[] = $font_variant->weight;
			$this->styles[] = $font_variant->style;
		}
		$this->weights = array_unique( $this->weights );
		$this->styles = array_unique( $this->styles );
	}

	/**
	 *	@param string $what What to get
	 *	@return mixed
	 */
	public function __get( $what ) {
		if ( in_array( $what, [ 'name', 'category', 'slug' ] ) ) {
			return $this->$what;
		} else if ( in_array( $what, [ 'variants', 'subsets', 'weights', 'styles' ] ) ) {
			return array_values( $this->$what );
		} else if ( 'type' === $what ) {
			return FontLibrary::$categoryType[$this->category];
		} else if ( 'family_css' === $what ) {
			return sprintf( '\'%s\', %s', $this->name, $this->type );
		}
	}

	/**
	 *	@return String css rules for font
	 */
	public function get_css( $weight = false, $style = false, $line_before = '', $line_after = '' ) {
		$css = '';
		$css .= $line_before . sprintf( 'font-family: %s;', $this->family_css ) . $line_after;
		if ( $weight && in_array( $weight, $this->weights ) ) {
			$css .= $line_before . sprintf( 'font-weight: %d;', $weight ) . $line_after;
		}
		if ( $style && in_array( $style, $this->styles ) ) {
			$css .= $line_before . sprintf( 'font-style: %s;', $style ) . $line_after;
		}
		return $css;
	}


	/**
	 *	@return String family=Font:prop1,prop2@val1,val2;val1,val2;....
	 */
	public function get_url_param( $all = true, $weight = false, $style = false ) {
		$vals = [];
		if ( $all ) {
			$props = ['ital','wght'];

			foreach ( $this->variants as $var ) {
				$vals[] = intval( $var->italic ) . ',' . $var->weight;
			}
		} else {
			$props = [];
			$val = [];
			if ( $style && in_array( $style, $this->styles ) ) {
				$props[] = 'ital';
				$val[] = intval( $style === 'italic' );
			}
			if ( $weight && in_array( $weight, $this->weights ) ) {
				$props[] = 'wght';
				$val[] = $weight;
			}
			if ( count($val ) ) {
				$vals[] = implode( ',', $val );
			}
		}
		sort($vals);
		return sprintf(
			'family=%1$s%2$s%3$s',
			str_replace( ' ', '+', $this->name ),
			count( $props )
				? ':' . implode( ',', $props )
				: '',
			count( $vals )
				? '@' . implode( ';', $vals )
				: ''
		);
	}


	/**
	 *	Whether a filter matches
	 *
	 *	@param array $filter [ 'family' => 'term*', 'styles' => 'italic'|['italic'|'normal'], 'weights' => 400|[400,700], 'category' => 'serif'|'sans-serif'|'display'|'handwriting'|'monospace', 'subsets' => 'latin'|['latin','latin-ext'] ]
	 *	@return bool
	 */
	public function matches( $filter ) {
		//

		foreach ( $filter as $prop => $val ) {
			if ( 'family' === $prop ) {
				// match name
				if ( ! strtolower( $this->name ) === strtolower( $val ) ) {
					return false;
				}
			} else if ( 'category' === $prop ) {
				// not in category
				if ( ! in_array( $this->category, (array) $val ) ) {
					return false;
				}
			} else if ( in_array( $prop, [ 'weights', 'styles', 'subsets' ] ) ) {
				// all styles/subsets/weights present

				if ( count( array_diff( (array) $val, $this->$prop ) ) !== 0 ) {
					return false;
				}
			}
		}
		return true;
	}

}
