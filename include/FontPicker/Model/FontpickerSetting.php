<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Model;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class FontpickerSetting {

	// base
	/** @var String */
	private $label;

	/** @var Array */
	private $selector;


	// UI
	/** @var Array */
	private $filter;

	/** @var Boolean */
	private $select_style;

	// font
	/** @var String */
	private $family;

	/** @var Null|Integer 100|200|300|...|900 */
	private $weight;

	/** @var Null|String normal|italic */
	private $style;

	/** @var Array styles to load */
	private $load_styles;


	/**
	 *	@param Array $data
	 */
	public function __construct( $data ) {
		$defaults = [
			'label' => '',
			'selector' => [],
			'filter' => [],
			'select_style' => false,
			'family' => '',
			'weight' => null,
			'style' => null,
			'load_styles' => [],
		];
		foreach ( $defaults as $prop => $val ) {
			$this->$prop = isset( $data[$prop] ) ? $data[$prop] : $val;
		}
	}


	public function css() {
		$css = implode( ',', $this->selector );
		$css .= '{';
			$css .= sprintf( 'font-family:"%s",%s;', $this->family, $category );
			if ( $this->select_style ) {
				if ( ! is_null( $this->weight ) ) {
					$css .= sprintf( 'font-weight:%d;', $this->weight );
				}
				if ( ! is_null( $this->style ) ) {
					$css .= sprintf( 'font-weight:%d;', $this->style );
				}
			}
		$css .= '}';
		return $css;
	}

	public function gfontapi() {
		return 'family='; // ...
	}

}
