<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Model;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;

class FontLibrary extends Core\Singleton {

	/** @var array font weight names */
	public static $weightNames = [
		'100' => 'Thin',
		'200' => 'ExtraLight',
		'300' => 'Light',
		'400' => 'Regular',
		'500' => 'Medium',
		'600' => 'SemiBold',
		'700' => 'Bold',
		'800' => 'ExtraBold',
		'900' => 'Black',
	];

	/** @var array font weight names */
	public static $styleNames = [
		'normal' => 'Normal',
		'italic' => 'Italic',
	];

	/** @var array font weight names */
	public static $categoryNames = [
		'sans-serif' => 'Sans-Serif',
		'serif' => 'Serif',
		'monospace' => 'Monospace',
		'display' => 'Display',
		'handwriting' => 'Handwriting',
	];

	/** @var array font weight names */
	public static $categoryType = [
		'sans-serif' => 'sans-serif',
		'serif' => 'serif',
		'monospace' => 'monospace',
		'display' => 'cursive',
		'handwriting' => 'cursive',
	];


	/** @var array font family items */
	private $items = [];

	/** @var array Font Categories */
	private $categories = [];

	/** @var array Font Subsets */
	private $subsets = [];

	/**
	 *	@param int $weight Numeric Weight
	 *	@return string The weight name
	 */
	public static function getWeightName( $weight ) {
		$weight = strval( $weight );
		if ( isset( self::$weightNames[ $weight ] ) ) {
			return self::$weightNames[ $weight ];
		}
		return '';
	}

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		$this->load();

		parent::__construct();

	}

	/**
	 *	Load library from json file
	 */
	private function load() {

		$core = Core\Core::instance();
		$library = false;
		$updir = wp_upload_dir();
		$check_paths = [
			$updir['basedir'] . '/google-font-library.json', // maybe a more recent version has been downloaded
			$core->get_plugin_dir() . '/json/google-fonts-lib.json', // fallback
		];
		$paths = apply_filters( 'font_picker_library_check_paths', $check_paths );
		foreach ( $paths as $path ) {
			if ( file_exists( $path ) ) {
				$library = json_decode( file_get_contents( $path ), false );
				break;
			}
		}

		if ( ! $library ) {
			// couldn't get library
			return false;
		}
		if ( ! isset( $library->items ) || ! is_array( $library->items ) ) {
			return false;
		}
		foreach ( $library->items as $item ) {
			$family = new FontFamily( $item );
			$this->items[] = $family;
			if ( ! isset( $this->categories[ $family->category ] ) ) {
				$this->categories[ $family->category ] = ucwords( $family->category, " \t\r\n\f\v-_" );
			}
			$this->subsets = array_merge( $this->subsets, $family->subsets );
			$this->subsets = array_unique( $this->subsets );
		}
	}

	/**
	 *	@return Boolean|FontFamily false if not found
	 */
	public function findFamily( $family_name ) {

		if ( empty( $family_name ) ) {
			return false;
		}

		foreach ( $this->items as $family ) {
			if ( $family->name === $family_name ) {
				return $family;
			}
		}
		return false;
	}

	/**
	 *	@return FontFamily[]
	 */
	public function items() {
		return $this->items;
	}

	/**
	 *	@return String[]
	 */
	public function subsets() {
		return $this->subsets;
	}

	/**
	 *	@param array $filter [ 'family' => 'term*', 'styles' => 'italic'|['italic'|'normal'], 'weights' => 400|[400,700], 'category' => 'serif'|'sans-serif'|'display'|'handwriting'|'monospace', 'subsets' => 'latin'|['latin','latin-ext'] ]
	 */
	public function filter( $filter ) {

		$filter = array_filter( $filter );
		if ( isset($filter['weights'] ) ) {
			$filter['weights'] = array_map('intval',$filter['weights']);
		}
		return array_filter( $this->items, function( $fontFamily ) use ( $filter ) {

			$match = $fontFamily->matches( $filter );
			// var_dump($fontFamily->name,$match);
			return $match;
		} );

	}

}
