<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Model;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

class FontVariant {

	/** @var int 100, 200, ... 900 */
	private $weight = 0;

	/** @var string italic|normal */
	private $style = '';

	/** @var FontFamily */
	private $family = null;


	/**
	 *	@param string $variant Variant identifier like 'regular' or '800i'
	 *	@param FontFamily $family
	 */
	public function __construct( $variant, $family ) {
		if ( 'regular' === $variant ) {
			$this->weight = 400;
			$this->style = 'normal';
		} else if ( 'italic' === $variant ) {
			$this->weight = 400;
			$this->style = 'italic';
		} else {
			$this->weight = intval( $variant );
			$this->style = ( false === strpos( $variant, 'i' ) ) ? 'normal' :  'italic';
		}
	}

	/**
	 *	@param string $what What to get
	 *	@return mixed
	 */
	public function __get( $what ) {
		if ( 'bold' === $what ) {
			return $this->weight === 700;
		} else if ( 'italic' === $what ) {
			return $this->style === 'italic';
		} else if ( 'styleName' === $what ) {
			return $this->style === 'italic' ? 'Italic' : '';
		} else if ( 'weightName' === $what ) {
			return FontLibrary::getWeightName( $this->weight ) . ( $this->italic ? ' Italic' : '' );;
		} else if ( 'name' === $what ) {
			return $this->weightName;
		} else if ( in_array( $what, [ 'style', 'weight' ] ) ) {
			return $this->$what;
		} else if ( 'id' === $what ) {
			return strval( $this->weight ) . ( $this->italic ? 'i' : '' );
		}
	}

}
