<?php
/**
 *	@package FontPicker\Asset
 *	@version 1.0.1
 *	2018-09-22
 */

namespace FontPicker\Asset;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;


/**
 *	Asset Class
 *
 *	Usage
 *	-----
 *	<?php
 *
 *	// will throw exception if 'js/some-js-file.js' is not there!
 *	$some_asset = Asset\Asset::get( 'js/some-js-file.js' )
 *		// wrapper to wp_localize_script()
 *		->localize( [
 *			'some_option'	=> 'some_value',
 *			'l10n'			=> [
 *				'hello'	=> __('World','wp-font-picker')
 *			],
 *		], 'l10n_varname' )
 *		->deps( 'jquery' ) // or ->deps( [ 'jquery','wp-backbone' ] )
 *		->footer( true ) // enqueue in footer
 *		->enqueue(); // actually enqueue script
 *
 */
class Asset {

	/**
	 *	@var string relative asset path
	 */
	private $asset;

	/**
	 *	@var string Absolute asset path
	 */
	private $path;

	/**
	 *	@var string Absolute asset url
	 */
	private $url;

	/**
	 *	@var array Dependencies
	 */
	private $deps = [];

	/**
	 *	@var array|boolean Localization
	 */
	private $in_footer = true;

	/**
	 *	@var string css|js
	 */
	private $type;

	/**
	 *	@var string
	 */
	private $handle;

	/**
	 *	@var string
	 */
	private $version;

	/**
	 *	@var string
	 */
	private $varname;

	/**
	 *	@var boolean
	 */
	private $localized;

	/**
	 *	@var array|boolean Localization
	 */
	private $l10n = false;

	/**
	 *	@var bool
	 */
	private $registered = false;

	/**
	 *	@var bool
	 */
	private $enqueued = false;

	/**
	 *	@var array args for wp_add_inline_*
	 */
	private $inline = [];

	/**
	 *	@var array args for wp_add_inline_*
	 */
	private $localize = [];

	/**
	 *	@var Core\Core
	 */
	private $core = null;

	static function get( $asset ) {
		return new self($asset);
	}


	public function __construct( $asset, $type = 'auto' ) {

		$this->core = Core\Core::instance();

		$this->asset = preg_replace( '/^(\/+)/', '', $asset ); // unleadingslashit
		if ( $type === 'auto' ) {
			$this->type = strtolower( pathinfo( $this->asset, PATHINFO_EXTENSION ) );
		} else {
			$this->type = $type;
		}
		$this->handle = $this->generate_handle();
		$this->varname = str_replace( '-', '_', $this->handle );
		$this->in_footer = $this->type === 'js';
		$this->version = $this->core->version();
		$this->locate();
	}

	public function set_version( $version ) {
		$this->version = $version;
		return $this;
	}

	/**
	 *	Generate script handle.
	 */
	private function generate_handle() {

		// bs11-öäü-js-some-path-file_name-öäü
		if ( $this->is_external() ) {

			$handle = sprintf( '%s-%s-%s',
				$this->core->get_prefix(),
				parse_url( $this->asset, PHP_URL_HOST ),
				parse_url( $this->asset,  PHP_URL_PATH )
			);
		} else {
			$asset = preg_replace( '/^(js|css)\//', '', $this->asset );

			$pi = pathinfo( $asset ); // öäü/js/some/path/file_name-öäü.js

			$handle = sprintf( '%s-%s-%s', $this->core->get_prefix(), $pi['dirname'], $pi['filename'] );//str_replace( '/', '-',  );
		}

		// ----js-some-path-file-name----
		$handle = preg_replace( '/[^a-z0-9_]/','-',  $handle );

		// js-some-path-file-name ... should do it
		$handle = trim( $handle, '-' );

		return $handle;
	}

	/**
	 *	Whether the asset is loaded from an external source
	 *
	 *	@return Boolean
	 */
	public function is_external() {
		$parsed_url = parse_url( $this->asset );

		if ( ! isset( $parsed_url['host'] ) ) {
			return false;
		}

		$parsed_home = parse_url( get_option( 'home' ) );

		return strtolower( $parsed_home['host'] ) !== strtolower( $parsed_url['host'] );
	}

	/**
	 *	Locate asset file
	 */
	private function locate() {
		// !!! must know plugin or theme !!!
		$check = $this->core->get_asset_roots();

		//
		if ( $this->is_external( $this->asset ) ) {
			$this->path = '';
			$this->url = $this->asset;
			return;
		}

		foreach ( $check as $root_path => $root_url ) {
			$root_path = untrailingslashit( $root_path );
			$root_url = untrailingslashit( $root_url );
			$path = $root_path . '/' . $this->asset;
			if ( file_exists( $path ) ) {
				$this->path = $path;
				$this->url = $root_url . '/' . $this->asset;
				return;
			}
		}
		if ( defined('WP_DEBUG') && WP_DEBUG ) {
			throw new \Exception( sprintf( 'Couldn\'t locate %s', $this->asset ) );
		}
	}


	/**
	 *	Set Dependencies
	 *
	 *	@param array $deps Dependencies
	 */
	public function deps( ...$deps ) {
		$this->deps = [];
		foreach ( $deps as $dep ) {
			$this->add_dep( $dep );
		}
		return $this;
	}

	/**
	 *	Add Dependency
	 *
	 *	@param Asset|array|string $dep Dependency slug(s) or Asset instance
	 */
	public function add_dep( $dep ) {
		if ( $dep instanceof self ) {
			$dep = $dep->handle;
		}
		if ( is_array( $dep ) ) {
			foreach ( $dep as $d ) {
				$this->add_dep($d);
			}
		} else {
			if ( ! in_array( $dep, $this->deps ) ) {
				$this->deps[] = $dep;
			}
		}
		return $this;
	}

	/**
	 *	Set Dependencies
	 *
	 *	@param boolean $in_footer Dependencies
	 */
	public function footer( $in_footer = true ) {
		$this->in_footer = $in_footer;
		return $this;
	}

	/**
	 *	Register asset
	 *	Wrapper for wp_register_[script|style]
	 */
	public function register( ) {
		if ( ! $this->registered ) {
			$fn = $this->type === 'js' ? 'wp_register_script' : 'wp_register_style';
			$args = [
				$this->handle,
				$this->url,
				$this->deps,
				null, //$this->core->version()
			];
			if ( $this->in_footer ) {
				$args[] = $this->in_footer;
			}
			call_user_func_array(
				$fn,
				$args
			);
			$this->registered = true;

		}
		return $this->_localize();
	}

	/**
	 *	Enqueue asset
	 *	Wrapper for wp_enqueue_[script|style]
	 *
	 *	@param string|array $deps Single Dependency or Dependencies array
	 */
	public function enqueue( $deps = [] ) {

		$fn = $this->type === 'js' ? 'wp_enqueue_script' : 'wp_enqueue_style';

		if ( ! $this->registered ) {
			$this->register( $deps );
		}

		call_user_func( $fn, $this->handle );

		$this->enqueued = true;

		$this->_add_inline();

		return $this->_localize();
	}

	/**
	 *	Localize
	 *	Wrapper for wp_localize_script
	 *
	 *	@param array $l10n
	 *	@param null|string $varname
	 */
	public function localize( $l10n = [], $varname = null ) {
		if ( $this->type !== 'js' ) {
			throw new \Exception( 'Can\'t localize stylesheet' );
		}
		$this->localized = false;

		if ( is_null( $varname ) ) {
			$varname = $this->varname;
		}
		if ( is_array( $l10n ) ) {
			$this->localize[$varname] = $l10n;
		}

		return $this->_localize();
	}

	/**
	 *	Maybe call wp_localize_script
	 */
	private function _localize( ) {
		if ( $this->registered && ! $this->localized ) {
			foreach ( array_keys( $this->localize ) as $varname ) {
				wp_localize_script( $this->handle, $varname, $this->localize[$varname] );
				unset( $this->localize[$varname] );
			}

			$this->localized = true;

		}
		return $this;
	}

	/**
	 *	Wrapper for wp_script_add_data
	 */
	public function add_data( $key, $value ) {
		wp_script_add_data( $this->handle, $key, $value );
		return $this;
	}

	public function add_inline( $data, $position = 'after' ) {
		$this->inline[] = [ $this->handle, $data, $position ];
		if ( $this->enqueued ) {
			$this->_add_inline();
		}
		return $this;
	}

	private function _add_inline() {
		while ( count( $this->inline ) ) {
			$inline_args = array_shift( $this->inline );
			$fn = 'js' === $this->type ? 'wp_add_inline_script' : 'wp_add_inline_style';
			call_user_func_array( $fn, $inline_args );
		}
		return $this;
	}

	/**
	 *	Wrapper for wp_script_add_data
	 */
	public function set_script_translations( $text_domain ) {
		if ( ! $this->registered ) {
			$this->register();
		}
		$path = pathinfo( $this->core->get_plugin_dir(), PATHINFO_DIRNAME );
		$path = $this->core->get_plugin_dir();

		wp_set_script_translations( $this->handle, $text_domain, $path . 'languages' );

		return $this;

	}

	/**
	 *	magic getter
	 */
	public function __get( $var ) {
		switch ( $var ) {
			case 'asset':
			case 'handle':
			case 'in_footer':
			case 'path':
			case 'url':
			case 'varname':
				return $this->$var;
			case 'deps':
				return array_values( $this->$var );
		}
	}

}
