<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.1
 *	2018-09-22
 */

namespace FontPicker\Core;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}
use FontPicker\Asset;
use FontPicker\Customizer;
use FontPicker\Model;

class Core extends Plugin implements CoreInterface {

	/** @var Asset\Asset */
	private $css = null;

	/** @var Array */
	private $fontpickers = [];

	/**
	 *	@inheritdoc
	 */
	protected function __construct() {

		add_action( 'customize_register', [ '\FontPicker\Customizer\Customize', 'instance' ] );
		add_action( 'customize_preview_init', [ '\FontPicker\Customizer\CustomizePreview', 'instance' ] );

		add_action( 'wp_enqueue_scripts',  [ $this, 'googlefont_dequeue_gfont' ], 20 ); // later than theme stylesheet!
		add_action( 'wp_enqueue_scripts' , [ $this, 'enqueue_assets' ], 30 ); // later than googlefont_dequeue_gfont

		add_action( 'acf/include_field_types', [ '\FontPicker\ACF\Fields\FontFamilyPicker', 'register' ] );

		$args = func_get_args();
		parent::__construct( ...$args );

	}

	/**
	 *	@action wp_enqueue_scripts
	 */
	public function googlefont_dequeue_gfont() {
		global $wp_styles;

		if ( is_a( $wp_styles, 'WP_Styles' ) ) {
			foreach( $wp_styles->registered as $handle => $dep ) {
				if ( ! is_null( $dep->src ) && preg_match( '/\/\/fonts\.googleapis\.com\/css/', $dep->src ) ) {
					wp_dequeue_style( $handle );
				}
			}
		}
	}


	/**
	 *	Load frontend styles and scripts
	 *
	 *	@action wp_enqueue_scripts
	 */
	public function enqueue_assets() {

		if ( $font_url = $this->get_font_url() ) {

			$this->css = Asset\Asset::get( $font_url );
			if ( $css = $this->get_css() ) {
				$this->css->add_inline( $this->get_css() );
			}
			$this->css->enqueue();
		}
	}

	/**
	 *	@return String
	 */
	public function get_font_url() {
		return $this->generate_font_url( $this->get_theme_mod() );
	}

	/**
	 *	@return Array
	 */
	private function get_theme_mod() {
		$mods = get_theme_mod( 'fontpicker', [] );
		return array_map( [ $this, 'sanitize_theme_mod' ], $mods );
	}

	/**
	 *	@param Array $mods [
	 *		[
	 *			'font_family'	=> (string),
	 *			'use_style	'	=> (bool),
	 *			'font_weight'	=> (string|int) 100|200|...|900
	 *			'font_style'	=> (string) 'normal'|'italic'
	 *		],
	 *		...
	 *	]
	 */
	public function generate_font_url( $mods ) {
		$params = [];
		foreach ( $mods as $mod ) {
			if ( empty( $mod['font_family'] ) ) {
				continue;
			}
			if ( ! isset( $params[ $mod['font_family'] ] ) || ! $mod['use_style'] ) {
				$params[ $mod['font_family'] ] = $this->generate_url_param( $mod );
			}
		}
		/**
		 *	@param Array $params [
		 *		<font-family> =>
		 * ]
		 */
		$params = apply_filters('fontpicker_enqueue_fonts', $params );
		if ( empty( $params ) ) {
			return false;
		}
		return sprintf( 'https://fonts.googleapis.com/css2?%s&display=swap', implode( '&', array_values( $params ) ) );
	}

	public function get_css() {
		$mods = $this->get_theme_mod();
		$css = '';

		foreach ( $mods as $mod ) {
			$mod = $this->sanitize_theme_mod( $mod );

			$css .= $this->generate_css($mod);
			$css .= "\n";
		}

		return $css;
	}

	/**
	 *	@param Array $mod [
	 *		[
	 *			'font_family'     => (string),
	 *			'use_style	'     => (bool),
	 *			'font_weight'     => (bool|string|int) 100|200|...|900
	 *			'font_style'      => (bool|string) 'normal'|'italic'
	 *			'selector'        => (string),
	 *			'custom_selector' => (string),
	 *		]
	 *	]
	 *	@return Array
	 */
	private function sanitize_theme_mod( $mod ) {
		return wp_parse_args( $mod, [
			'font_family'     => '',
			'use_style'       => false,
			'font_weight'     => false,
			'font_style'      => false,
			'selector'        => '',
			'custom_selector' => '',
		]);
	}

	/**
	 *	@param Array $mod [
	 *		[
	 *			'font_family'	=> (string),
	 *			'use_style	'	=> (bool),
	 *			'font_weight'	=> (string|int) 100|200|...|900
	 *			'font_style'	=> (string) 'normal'|'italic'
	 *		]
	 *	]
	 *	@return String family=Font:prop1,prop2@val1,val2;val1,val2;....
	 */
	private function generate_url_param( $mod ) {
		$lib = Model\FontLibrary::instance();
		$font = $lib->findFamily( $mod['font_family'] );
		if ( ! $font ) {
			return '';
		}
		return $font->get_url_param( ! $mod['use_style'], $mod['font_weight'], $mod['font_style'] );

	}

	private function generate_css( $mod ) {
		$lib = Model\FontLibrary::instance();
		$font = $lib->findFamily( $mod['font_family'] );

		if ( ! $font ) {
			return '';
		}
		$css = '';
		if ( '' === $mod['selector'] ) {
			if ( empty( $mod['custom_selector'] ) ) {
				return '';
			}
			$css .= $mod['custom_selector'];
		} else {
			$css .= $mod['selector'];
		}

		$css .= '{' . "\n";
		$css .= $font->get_css( $mod['font_weight'], $mod['font_style'], "\t", "\n" );
		$css .= '}' . "\n";
		return $css;
	}

	/**
	 *	@inheritdoc
	 */
	public function upgrade( $new_version, $old_version ) {
		if ( version_compare( $old_version, '0.0.8', '<=' ) ) {

			$selectors = get_option( 'googlefont_selectors' );

			$new_mod = [];
			if ( is_array( $selectors ) ) {
				foreach ( $selectors as $selector ) {
					/* @var String like Source+Sans+Pro:regular,italic,700,700italic */
					$old_mod = get_theme_mod($selector['name']);
					@list( $family, $styles ) = explode( ':', $old_mod );
					$weight = $style = false; // unknown

					if ( $selector['show_styles'] ) {
						$weight = intval( $styles ) ? intval( $styles ) : 400;
						$style = strpos( $styles, 'italic' ) !== false ? 'italic' : false;
					}

					$new_mod[ $selector['name'] ] = [
						'name' => $selector['name'],
						'label' => $selector['label'],
						'selector' => '',
						'custom_selector' => $selector['css_selector'],
						'use_style' => boolval( $selector['show_styles'] ),

						'font_weight' => $weight,
						'font_style' => $style,
						'font_family' => str_replace( '+', ' ', $family ),
					];

					remove_theme_mod( $selector['name'] );
				}
			}
			set_theme_mod( 'fontpicker', $new_mod );

			delete_option( '_googlefont_fontlist' );
			delete_option( 'googlefont_refresh_period' );
			delete_option( 'googlefont_subset' );
			delete_option( 'googlefont_api_key' );
			delete_option( 'googlefont_selectors' );
		}
	}
}
