<?php
/**
 *	@package FontPicker\Core
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Core;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}


interface ComponentInterface {

	/**
	 *	Called on Plugin activation
	 *
	 *	@return Array [
	 *		'success'	=> bool,
	 *		'messages'	=> array,
	 *	]
	 */
	public function activate();

	/**
	 *	Called on Plugin upgrade
	 *	@param	String	$new_version
	 *	@param	String	$old_version
	 *	@return Array [
	 *		'success'	=> bool,
	 *		'messages'	=> array,
	 *	]
	 */
	public function upgrade( $new_version, $old_version );

	/**
	 *	Called on Plugin deactivation
	 *	@return Array [
	 *		'success'	=> bool,
	 *		'messages'	=> array,
	 *	]
	 */
	public function deactivate();

	/**
	 *	Called on Plugin uninstall
	 *	@param	String	$new_version
	 *	@param	String	$old_version
	 *	@return Array [
	 *		'success'	=> bool,
	 *		'messages'	=> array,
	 *	]
	 */
	public static function uninstall();

}
