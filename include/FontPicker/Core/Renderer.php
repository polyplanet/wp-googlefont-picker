<?php

namespace FontPicker\Core;

use FontPicker\Model;


class Renderer {

	/**
	 *	@param String $input_name
	 *	@param String $input_id
	 *	@param String $selected_family
	 *	@param String $input_extra_attr
	 *	@param Array $filter_fonts @see Model\FontLibrary::filter( $filter )
	 */
	public static function render_font_picker( $input_name, $input_id, $selected_family = false, $input_extra_attr = '', $font_filter = [ 'subsets' => 'latin' ] ) {

		$font_filter = array_filter( $font_filter );
		$font_lib = Model\FontLibrary::instance();


		$favorites = get_user_option( 'googlefont_favorites' ); // legacy name...

		if ( ! $favorites ) {
			$favorites = [];
		}

		$all_filter_props = [ 'favorite' => __( 'Favorites', 'wp-font-picker' ) ];
		if ( ! isset( $font_filter['weights'] ) ) {
			$all_filter_props += Model\FontLibrary::$weightNames;
		}
		if ( ! isset( $font_filter['styles'] ) ) {
			$all_filter_props += Model\FontLibrary::$styleNames;
			unset($all_filter_props['normal']);
		}


		foreach ( $all_filter_props as $prop => $label ) {
			printf(
				'<input id="%s" type="checkbox" class="fontpicker-filter-input" value="%s" />',
				$input_id . '-filter-' . $prop,
				esc_attr( $prop )
			);
		}


		if ( ! isset( $font_filter['category'] ) ) {
			$cat_props = [ '__allcats' => __('All categories', 'wp-font-picker') ] + Model\FontLibrary::$categoryNames;

			foreach ( $cat_props as $cat => $label ) {
				printf(
					'<input id="%s" type="radio" name="%s" class="fontpicker-filter-input" value="%s" %s />',
					$input_id . '-filter-' . $cat,
					'fontpicker-filter-category-' . $input_id,
					esc_attr( $cat ),
					checked( $cat, '__allcats', false )
				);
			}
		}

		?>
		<input type="checkbox" class="fontpicker-filter-expanded" id="fontpicker-expand-filter-<?php echo $input_id; ?>" />
		<label class="fontpicker-filter-handle" for="fontpicker-expand-filter-<?php echo $input_id; ?>">
			<span class="fontpicker-filter-title">
				<span class="dashicons dashicons-filter"></span>
				<?php esc_html_e('Filter','wp-font-picker' ); ?>
			</span>
		</label>
		<span class="fontpicker-filter">
			<span class="fontpicker-filter-section -search">
				<input type="search" placeholder="<?php esc_attr_e('Search &hellip;', 'wp-font-picker' ); ?>" />
				<span class="dashicons dashicons-search"></span>
			</span>
			<span class="fontpicker-filter-section -favorites">
				<?php
				printf(
					'<label for="%1$s" data-filter-prop="%2$s"><span class="dashicons dashicons-star-filled"></span>%3$s</label>',
					$input_id . '-filter-favorite',
					esc_attr( 'favorite' ),
					esc_html( $all_filter_props['favorite'] )
				);

				?>
			</span>
			<?php if ( ! isset( $font_filter['category'] ) ) { ?>
				<span class="fontpicker-filter-section -categories">
					<h4><?php esc_html_e('Categories','wp-font-picker'); ?></h4>
					<?php
					foreach ( $cat_props as $prop => $label ) {
						printf(
							'<label for="%1$s" data-filter-prop="%2$s">%3$s</label>',
							$input_id . '-filter-' . $prop,
							esc_attr( $prop ),
							esc_html( $label )
						);
					}
					?>
				</span>
			<?php } ?>
			<?php
				$style_props = array_intersect( $all_filter_props,  Model\FontLibrary::$weightNames + Model\FontLibrary::$styleNames );
			 	if ( ! empty( $style_props ) ) { ?>

				<span class="fontpicker-filter-section -weights">
					<h4><?php esc_html_e('Available Variants','wp-font-picker'); ?></h4>
					<?php
					foreach ( $style_props as $prop => $label ) {
						if ( ! isset( $all_filter_props[$prop] ) ) {
							continue;
						}
						printf(
							'<label for="%1$s" data-filter-prop="%2$s">%3$s</label>',
							$input_id . '-filter-' . $prop,
							esc_attr( $prop ),
							esc_html( $label )
						);
					}

					?>
				</span>
			<?php } ?>

		</span>



		<?php

		self::render_font_picker_items( $input_name, $input_id, $selected_family, $input_extra_attr, $font_filter );
	}

	public static function render_font_picker_items( $input_name, $input_id, $selected_family = false, $input_extra_attr = '', $font_filter = [ 'subsets' => 'latin' ] ) {

		$font_filter = array_filter( $font_filter );
		$font_lib = Model\FontLibrary::instance();

		$items = $font_lib->filter( $font_filter );

		if ( ! empty( $selected_family ) ) {
			$selected_items = array_filter($items,function($item) use ( $selected_family ) {
				return $item->matches( [ 'family' => $selected_family ] );
			});
			if ( ! count( $selected_items ) ) {
				$selected_family = '';
			}
		}


		$favorites = get_user_option( 'googlefont_favorites' ); // legacy name...

		if ( ! $favorites ) {
			$favorites = [];
		}

		?>
		<span class="fontpicker-item fontpicker-item-none">
			<input
				id="<?php echo $input_id . '-radio-__none'; ?>"
				type="radio"
				<?php echo $input_extra_attr; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				value=""
				name="<?php echo esc_attr( $input_name ); ?>"
				class="fontpicker-value"
				<?php checked( $selected_family, '', true ); ?>
				/>
				<label for="<?php echo $input_id . '-radio-__none'; ?>">
					<span class="fontpicker-name">
						<?php echo esc_html_e( '– None –', 'wp-font-picker' ); ?>
					</span>
					<span class="fontpicker-meta">
						&nbsp;
					</span>
				</label>
		</span>
		<?php foreach ( $items as $font ) { ?>
			<span class="fontpicker-item"
			<?php
				$is_fav = in_array( $font->name , $favorites );
				$font_filter_props = array_merge( $font->weights, $font->styles, [ $font->category ] );
				if ( $is_fav ) {
					$font_filter_props[] = 'favorite';
				}
				printf( ' data-filter-props="|%s|"', esc_attr( implode( '|', $font_filter_props ) ));
				printf( ' data-name="%s"', esc_attr( strtolower( $font->name ) ) );
			?>
			>
				<input
					id="<?php echo $input_id . sanitize_key( '-radio-' . $font->name ); ?>"
					type="radio"
					<?php echo $input_extra_attr; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					value="<?php echo esc_attr( $font->name ); ?>"
					name="<?php echo esc_attr( $input_name ); ?>"
					class="fontpicker-value"
					<?php checked( $selected_family, $font->name, true ); ?>
					/>
				<label tabindex for="<?php echo $input_id . sanitize_key( '-radio-' . $font->name ); ?>">
					<span class="favorite-box">
						<button type="button" class="toggle-favorite button-link dashicons dashicons-star-empty">
							<span class="screen-reader-text">
								<?php
								if ( $is_fav ) {
									esc_html_e( 'Remove from favorites', 'wp-googefont-picker' );
								} else {
									esc_html_e( 'Add to favorites', 'wp-googefont-picker' );
								}
								?>
							</span>
						</button>
					</span>
					<span class="fontpicker-name" <?php
						printf( ' style="%s"', esc_attr( $font->get_css() ) );
						printf( ' data-url-param="%s"', esc_attr( $font->get_url_param() ) );
						printf( ' data-name="%s"', esc_attr( $font->name ) );
					?>>
						<?php echo esc_html( $font->name ); ?>
					</span>
					<span class="fontpicker-meta">
						<span class="fontpicker-meta-name"><?php echo esc_html( $font->name ); ?></span>
						|
						<?php echo esc_html( $font->category ) ?>
						|
						<?php
						echo esc_html(
							sprintf(
								/* translators: number of available variants */
								_n( '%d variant', '%d variants', count( $font->variants ), 'wp-font-picker' ),
								count( $font->variants )
							)
						);
						?>
					</span>
				</label>
			</span>
		<?php }

	}

}
