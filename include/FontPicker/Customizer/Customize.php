<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Admin;
use FontPicker\Ajax;
use FontPicker\Asset;
use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class Customize extends Core\Singleton {

	/** @var Asset\Asset */
	private $css = null;

	/** @var Asset\Asset */
	private $js = null;

	/** @var WP_Customize_Manager */
	private $wp_customize;

	/** @var Array */
	private $font_family_choices = null;

	/**
	 *	@inheritdoc
	 */
	protected function __construct( $wp_customize ) {

		$admin = Admin\Admin::instance();

		if ( 'customize_register' === current_filter() ) {
			$this->register( $wp_customize );
		} else {
			add_action( 'customize_register', [ $this , 'register' ] );
		}
		add_action( 'customize_controls_enqueue_scripts', [ $this , 'enqueue_assets' ] );

		$admin = Admin\Admin::instance();

		$this->css = Asset\Asset::get( 'css/fontpicker.css' );
		$this->js = Asset\Asset::get( 'js/customize/font-picker.js' )
			->add_dep('jquery')
			->add_dep('customize-controls')
			->localize([
				'l10n'		 => [
					'newFontpicker'	=> __('New Fontpicker','wp-font-picker'),
					'name'			=> __('Name','wp-font-picker'),
					'elements'		=> __('Elements','wp-font-picker'),
					'cssSelector'	=> __('CSS selector','wp-font-picker'),
				],
				'selectors' => $admin->get_available_css_selectors(),
			],'fontpicker_customize');

	}




	/**
	 *	@param WP_Customize $wp_customize
	 *	@action customize_register
	 */
	public function register( $wp_customize ) {

		$allow_new_fontpicker = apply_filters( 'fontpicker_allow_add_new', true );

		$this->wp_customize = $wp_customize;

		$core = Core\Core::instance();

		$this->wp_customize->register_panel_type( 'FontPicker\Customizer\FontPickerPanel' );
		$this->wp_customize->register_section_type( 'FontPicker\Customizer\FontPickerSection' );
		$this->wp_customize->register_control_type( 'FontPicker\Customizer\FontPickerControl' );
		$this->wp_customize->register_section_type( 'FontPicker\Customizer\AddNewFontPickerSection' );
		$this->wp_customize->register_control_type( 'FontPicker\Customizer\AddNewFontPickerControl' );



		$panel = $this->wp_customize->add_panel( 'fontpicker', [
			'id' => 'fontpicker',
			//'type' => 'default',
			'title' => __( 'Font Picker', 'wp-font-picker'),
			'priority' => 100,
			// 'capability' => 'edit_theme_options',
			'theme_supports' => [],
			'description' => '',
			'active_callback' => '__return_true',
			'type' => 'fontpicker',

		] );

		if ( $allow_new_fontpicker ) {

			$panel = $this->wp_customize->add_section( 'new_fontpicker', [
				'id' => 'new_fontpicker',
				'type' => 'new_fontpicker',
				'title' => __( 'New Font Picker', 'wp-font-picker'),
				'priority' => 1000,
				'panel' => 'fontpicker',
				'active_callback' => '__return_true',
			] );
			$this->wp_customize->add_setting( 'new_fontpicker', [ 'transport' => 'postMessage' ] );
			$this->wp_customize->add_control(
				'new_fontpicker',
				[
					'section' => 'new_fontpicker',
					'type' => 'new_fontpicker',
				]
			);

		}


		// add clone section
		$this->add_fontpicker_control( '__new_fontpicker__', [], [
			'active_callback' => '__return_false',
		] );
		$mods = (array) get_theme_mod( 'fontpicker' );

		foreach ( array_filter( $mods ) as $key => $mod ) {
			$this->add_fontpicker_control( $key, $mod );
		}
	}

	/**
	 *	Add a fontpicker section
	 *
	 *	@param String $mod_key
	 *	@param Array $mod
	 *	@param Array $section_args
	 */
	private function add_fontpicker_control( $mod_key, $mod, $section_args = [] ) {
		$admin = Admin\Admin::instance();
		$mod = wp_parse_args( $mod, [ 'label' => __('New Fontpicker', 'wp-font-picker') ] );

		$section_id = $setting_prefix = sprintf( 'fontpicker[%s]', $mod_key );

		$section = $this->wp_customize->add_section( $section_id, wp_parse_args( $section_args, [
			'id' => $section_id,
			'title' => $mod['label'],
			'panel' => 'fontpicker',
			'type' => 'fontpicker',
		]) );

		// active cbs
		$selector_active_cb = function( $wp_customize ) use ($mod_key) {
			$mod = get_theme_mod('fontpicker');
			return '' === $mod[$mod_key]['selector'];
		};
		$use_style_active_cb = function( $wp_customize ) use ($mod_key) {
			$mod = get_theme_mod('fontpicker');
			return boolval( $mod[$mod_key]['use_style'] );
		};

		$css_selectors = $admin->get_available_css_selectors();
		$allow_new_fontpicker = apply_filters( 'fontpicker_allow_add_new', true );

		$settings = [];
		$settings['name'] = [
			'control' => [
				'type' => 'hidden',
			],
			'setting' => [
				'transport' => 'postMessage',
				'default' => $mod_key,
			],
		];
		if ( $allow_new_fontpicker ) {
			$settings['label'] = [
				'control' => [
					'label' => __('Name','wp-font-picker'),
				],
				'setting' => [
					'transport' => 'postMessage'
				],
			];
		} else {
			$settings['label'] = [
				'control' => [
					'type' => 'hidden',
				],
			];
		}
		if ( is_array( $css_selectors ) ) {
			$settings['selector'] = [
				'control' => [
					'label' => __('Affected Elements','wp-font-picker'),
					'type' => 'select',
					'choices' => $css_selectors,
				],
				'setting' => [
					'default' => $admin->get_default_css_selector(),
				],
			];
			$settings['custom_selector'] = [
				'control' => [
					'label' => __('CSS selector','wp-font-picker'),
					'active_callback' => $selector_active_cb,
				],
			];
		} else {
			$settings['selector'] = [
				'control' => [
					'type' => 'hidden',
					'label' => '',
				],
			];
			$settings['custom_selector'] = [
				'control' => [
					'type' => 'hidden',
					'label' => '',
				],
			];
		}
		$settings['use_style'] = [
			'control' => [
				'label' => __('Use Style','wp-font-picker'),
				'type' => 'checkbox',
			],
			'setting' => [
				'default' => false,
			],
		];
		$settings['font_weight'] = [
			'control' => [
				'type' => 'select',
				'choices' => [ '' => __( '– Font weight –', 'wp-font-picker' ) ] + Model\FontLibrary::$weightNames,
				'active_callback' => $use_style_active_cb,
			],
			'setting' => [
			],
		];
		$settings['font_style'] = [
			'control' => [
				'type' => 'select',
				'choices' => [
					'' => __( '– Font style –', 'wp-font-picker' ),
					'normal' => __( 'Normal', 'wp-font-picker' ),
					'italic' => __( 'Italic', 'wp-font-picker' ),
				],
				'active_callback' => $use_style_active_cb,
			],
			'setting' => [
				'value' => 'italic',
			],
		];
		$settings['font_family'] = [
			'control' => [
				'label' => __('Font family','wp-font-picker'),
				'type' => 'fontpicker',
			],
			'setting' => [
				'default' => '',
			],
		];

		foreach ( $settings as $key => $options ) {
			$options = wp_parse_args($options, [ 'setting' => [], 'control' => [] ] );
			//*
			$setting = $this->wp_customize->add_setting(
				$setting_prefix.'['.$key.']',
				wp_parse_args( $options['setting'], [
					'transport' => 'refresh',
				])
			);
			$control = $this->wp_customize->add_control(
				$setting_prefix.'['.$key.']',
				wp_parse_args($options['control'], [
					'section' => $section_id,
				])
			);
			/*/
			$control = $this->wp_customize->add_control(
				$setting_prefix.'['.$key.']',
				wp_parse_args($options['control'], [
					'section' => $section_id,
					'setting' => 'fontpicker',
				])
			);
			//*/

		}
		return $section;
	}



	/**
	 *	@action customize_controls_enqueue_scripts
	 */
	public function enqueue_assets() {
		$this->css->enqueue();
		$admin = Admin\Admin::instance();

		$this->js
			->add_dep( $admin->fontpicker_js )
			->enqueue();
//		wp_enqueue_script( 'googlefont-theme-customizer', plugins_url( '/js/googlefont-customizer.js' , dirname(__FILE__) ) , array('jquery') ) ;
//		wp_enqueue_style( 'googlefont-theme-customizer', plugins_url( '/css/googlefont-customizer.css' , dirname(__FILE__) )  );

	}

}
