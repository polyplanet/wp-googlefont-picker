<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class FontPickerControl extends \WP_Customize_Control {

	public $type = 'fontpicker';

	/**
	 *	@inheritdoc
	 */
	public function render_content() {}

	/**
	 *	@inheritdoc
	 */
	public function content_template() {

		$input_id         = '_customize-input-{{data.instanceNumber}}-' . $this->id;
		$description_id   = '_customize-description-{{data.instanceNumber}}-' . $this->id;
		$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';

		$name = '{{data.section}}[font_family]';

		?>
		<h3><?php esc_html_e('Fonts','wp-font-picker' ); ?></h3>
		<?php

		return Core\Renderer::render_font_picker( $name, $input_id, false, $describedby_attr . $this->get_link() );

	}


}
