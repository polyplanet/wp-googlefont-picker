<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class FontPickerSection extends \WP_Customize_Section {

	/**
	 * Type of this panel.
	 *
	 * @since 4.1.0
	 * @var string
	 */
	public $type = 'fontpicker';

	/**
	 *	@inheritdoc
	 */
	protected function render_template() {
 		?>
 		<li id="accordion-section-{{ data.id }}" class="accordion-section control-section control-section-{{ data.type }}">
 			<h3 class="accordion-section-title" tabindex="0">
 				<span class="label-text">{{ data.title }}</span>
 				<span class="screen-reader-text"><?php esc_html_e( 'Press return or enter to open this section', 'wp-font-picker' ); ?></span>
 			</h3>
 			<ul class="accordion-section-content">
 				<li class="customize-section-description-container section-meta <# if ( data.description_hidden ) { #>customize-info<# } #>">
 					<div class="customize-section-title">
 						<button class="customize-section-back" tabindex="-1">
 							<span class="screen-reader-text"><?php esc_html_e( 'Back', 'wp-font-picker' ); ?></span>
 						</button>
 						<h3>
 							<span class="customize-action">
 								{{{ data.customizeAction }}}
 							</span>
 							<span class="label-text">{{ data.title }}</span>
							<button type="button" class="button-link button-link-delete">
								<?php esc_html_e( 'Delete Fontpicker', 'wp-font-picker' ); ?>
							</button>
 						</h3>
 						<# if ( data.description && data.description_hidden ) { #>
 							<button type="button" class="customize-help-toggle dashicons dashicons-editor-help" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Help', 'wp-font-picker'  ); ?></span></button>
 							<div class="description customize-section-description">
 								{{{ data.description }}}
 							</div>
 						<# } #>

 						<div class="customize-control-notifications-container"></div>
 					</div>

 					<# if ( data.description && ! data.description_hidden ) { #>
 						<div class="description customize-section-description">
 							{{{ data.description }}}
 						</div>
 					<# } #>
 				</li>
 			</ul>
 		</li>
 		<?php
 	}

}
