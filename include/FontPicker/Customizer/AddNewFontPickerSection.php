<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer section
 */
class AddNewFontPickerSection extends \WP_Customize_Section {

	public $priority = 1000;

	/**
	 * Section type.
	 *
	 * @since 4.2.0
	 * @var string
	 */
	public $type = 'new_fontpicker';

	protected function render_template() {
		?>
		<p class="description no-fontpicker">
			<?php esc_html_e( 'There are no fontpickers …', 'wp-googefont-picker' ) ?>
		</p>
		<li id="accordion-section-{{ data.id }}" class="accordion-section control-section control-section-{{ data.type }}">
			<span class="accordion-section-title button" tabindex="0">
				<span class="dashicons dashicons-plus"></span>
				{{ data.title }}
			</span>
			<ul class="accordion-section-content">
				<li class="customize-section-description-container section-meta <# if ( data.description_hidden ) { #>customize-info<# } #>">
					<div class="customize-section-title">
						<button class="customize-section-back" tabindex="-1">
							<span class="screen-reader-text"><?php esc_html_e( 'Back', 'wp-googefont-picker' ); ?></span>
						</button>
						<h3>
							<span class="customize-action">
								{{{ data.customizeAction }}}
							</span>
							{{ data.title }}
						</h3>
						<# if ( data.description && data.description_hidden ) { #>
							<button type="button" class="customize-help-toggle dashicons dashicons-editor-help" aria-expanded="false"><span class="screen-reader-text"><?php esc_html_e( 'Help', 'wp-googefont-picker' ); ?></span></button>
							<div class="description customize-section-description">
								{{{ data.description }}}
							</div>
						<# } #>

						<div class="customize-control-notifications-container"></div>
					</div>

					<# if ( data.description && ! data.description_hidden ) { #>
						<div class="description customize-section-description">
							{{{ data.description }}}
						</div>
					<# } #>
					<?php /*
					<div class="fontpicker-create-controls customize-control">
						<p>
							<label for="fontpicker-create-input-name-{{ data.instanceNumber }}" class="customize-control-title">
								<?php esc_html_e('Name','wp-font-picker'); ?>
							</label>
							<span class="customize-control-description" id="fontpicker-create-description-{{ data.instanceNumber }}">
								<?php esc_html_e('Enter a name for your font picker.','wp-googefont-picker'); ?>
							</span>
							<input id="fontpicker-create-input-name-{{ data.instanceNumber }}" type="text" aria-describedby="fontpicker-create-description-{{ data.instanceNumber }}">
						</p>
						<p>
							<button type="button" class="button fontpicker-create-button">
								<?php esc_html_e('Create Font Picker','wp-font-picker'); ?>
							</button>
						</p>
					</div>
					*/ ?>
				</li>
			</ul>
		</li>
		<?php
	}

}
