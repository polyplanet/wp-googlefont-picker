<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class AddNewFontPickerControl extends \WP_Customize_Control {

	public $type = 'new_fontpicker';

	/**
	 *	@inheritdoc
	 */
	public function render_content() {}

	/**
	 *	@inheritdoc
	 */
	public function content_template() {
		?>
		<p>
			<label for="fontpicker-create-input-name-{{ data.instanceNumber }}" class="customize-control-title">
				<?php esc_html_e('Name','wp-font-picker'); ?>
			</label>
			<span class="customize-control-description" id="fontpicker-create-description-{{ data.instanceNumber }}">
				<?php esc_html_e('Enter a name for your font picker.','wp-googefont-picker'); ?>
			</span>
			<input id="fontpicker-create-input-name-{{ data.instanceNumber }}" class="new-fontpicker-name" type="text" aria-describedby="fontpicker-create-description-{{ data.instanceNumber }}">
		</p>
		<p>
			<button type="button" class="button fontpicker-create-button">
				<?php esc_html_e('Create Font Picker','wp-font-picker'); ?>
			</button>
		</p>
		<?php

	}


}
