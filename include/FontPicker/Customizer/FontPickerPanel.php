<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class FontPickerPanel extends \WP_Customize_Panel {

	/**
	 * Type of this panel.
	 *
	 * @since 4.1.0
	 * @var string
	 */
	public $type = 'fontpicker';

	/**
	 *	@inheritdoc
	 */
	protected function content_template() {
		parent::content_template();
		// add thing button goes here!
		return;
		?>
		<li class="fontpicker-add">
			<button type="button" class="button fontpicker-add-button">
				<?php esc_html_e('Add Fontpicker'); ?>
			</button>
		</li>
		<?php
	}
}
