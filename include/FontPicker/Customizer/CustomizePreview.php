<?php
/**
 *	@package FontPicker\Customizer
 *	@version 1.0.0
 *	2018-09-22
 */

namespace FontPicker\Customizer;

if ( ! defined('ABSPATH') ) {
	die('FU!');
}

use FontPicker\Admin;
use FontPicker\Ajax;
use FontPicker\Asset;
use FontPicker\Core;
use FontPicker\Model;

/**
 *	Font picker Customizer control
 */
class CustomizePreview extends Core\Singleton {

	/**
	 *	@inheritdoc
	 */
	protected function __construct( $wp_customize ) {
		$this->wp_customize = $wp_customize;
	}


}
