��    %      D  5   l      @     A     X  
   j     u     �     �  	   �     �  
   �     �  
   �     �     �  	   �                       	   &     0     5  	   <     F     K     [     j  *   q     �     �     �     �  
   �  	   �     �     �       �       �     �  
   �     �     �     �             
        %     :     F     Z  	   c     m  
   t       	   �     �     �     �  
   �     �     �     �     �  1   �     "	     1	     ?	     M	     [	     g	     x	     �	     �	           
                                                 %          #                      $                        	                       "                !                        %d variant %d variants Affected Elements All Titles All categories Available Variants Back Base Font CSS selector Categories Create Font Picker Custom CSS Delete Fontpicker Elements Favorites Filter Font Picker Font family Fonts Headlines Help Italic Main Menu Name New Font Picker New Fontpicker Normal Press return or enter to open this section Search &hellip; Site Content Site Footer Site Header Site Title Use Style – Font style – – Font weight – – None – Project-Id-Version: WP GoogleFont Picker v0.0.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-googlefont-picker
PO-Revision-Date: 2020-12-01 11:37+0100
Last-Translator: admin <joern@flyingletters.com>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.4.1
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 %d Variante %d Varianten Elemente Alle Titel Alle Kategorien Verfügbare Varianten Zurück Grundschrift CSS-Selektor Kategorien Fontpicker erstellen Eigenes CSS Fontpicker löschen Elemente Favoriten Filter Fontpicker Schriftfamilie Schriften Überschriften Hilfe Kursiv Hauptmenü Name Neue Schriftauswahl Neuer Fontpicker Normal Zum Öffnen dieses Bereichs Eingabetaste drücken Suche &hellip; Seiten-Inhalt Seiten-Footer Seiten-Header Seitentitel Styles verwenden – Stil – – Fettheit – – Keine – 