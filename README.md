WP Font Picker
===============

TODO
----
 - [ ] Maybe Fix Fluida Compatibility


Installation
------------

### Production (using Github Updater – recommended for Multisite)
 - Install [Andy Fragen's GitHub Updater](https://github.com/afragen/github-updater) first.
 - In WP Admin go to Settings / GitHub Updater / Install Plugin. Enter `/wp-font-picker` as a Plugin-URI.

### Using Composer
```
composer require /wp-font-picker
```

### Development
 - cd into your plugin directory
 - $ `git clone `
 - $ `cd wp-font-picker`
 - $ `npm install && npm run dev`
